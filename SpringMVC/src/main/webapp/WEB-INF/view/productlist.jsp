<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row g-3 align-items-center">
            <div class="col-auto">
                <a href="" class="btn btn-warning" style="margin: 20px;"><i class="fas fa-shopping-basket"></i></a>
            </div>
            <div class="col-auto">
                <p>${productCount}</p>
            </div>
        </div>
        <div class="row">
            <c:forEach var="product" items="${productList}">
                <div class="col-xs-1-12">
                    <div class="card">
                    <div class="card-body">
                        <h3 class="card-title"><c:out value="${product.getName()}"/></h3>
                        <p class="card-text"><c:out value="${product.getPrice()}"/></p>
                        <form action="add" method="post">
                            <input type="hidden" value="${product.getId()}" name="productId"></input>
                            <input type="submit" class="btn btn-success" style="float: right;"><i class="fas fa-cart-plus"></i></input>
                        </form>
                    </div>
                    </div>
                </div>
          </c:forEach>
        </div>
    </div>
</body>
</html>