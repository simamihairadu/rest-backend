package net.gsdgroup.controller;

import net.gsdgroup.data.Product;
import net.gsdgroup.persistence.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/products")
    public List<Product> getAllProducts(){

        return productRepository.getAll(Product.class);
    }
/*    static List<Product> products;
    private final DemoService demoService;
    private ProductRepository productRepository;
    private CompositeProduct basket;



    @Autowired
    public ProductController(DemoService demoService) {
        this.demoService = demoService;
        productRepository = new ProductRepository();
        basket = new CompositeProduct();
    }

    @GetMapping("welcome")
    public String welcome(Model model){
        model.addAttribute("productList",productRepository.getAll());
        model.addAttribute("productCount",CompositeProduct.productCount);
        return "productlist";
    }

    @PostMapping("add")
    public String addToBasket(Model model, @RequestParam("productId") int productId){
        basket.add(productRepository.getProduct(productId));
        model.addAttribute("productList",productRepository.getAll());
        model.addAttribute("productCount",CompositeProduct.productCount);
        return "productlist";
    }

    @ResponseBody
    @GetMapping("/products")
    public List<Product> getProducts(){
        return productRepository.getAll();
    }

    @ResponseBody
    @PostMapping("/products")
    public Product getProduct(@RequestParam("productId") int productId){
        return productRepository.getProduct(productId);
    }

    @ResponseBody
    @PostMapping("/products/add")
    public void addProduct(@RequestParam("productName") String productName,@RequestParam("price") float price){
        productRepository.addProduct(productName,price);
    }*/
}
