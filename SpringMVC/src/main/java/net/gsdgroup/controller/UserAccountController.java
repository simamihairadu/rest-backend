package net.gsdgroup.controller;

import net.gsdgroup.data.UserAccount;
import net.gsdgroup.persistence.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserAccountController {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @PostMapping("/add")
    public void addUser(@RequestBody UserAccount user){

        userAccountRepository.add(user);
    }

    @PostMapping("/delete")
    public void deleteUser(@RequestParam("id") int id){

        userAccountRepository.delete(UserAccount.class, id);
    }

    @GetMapping("/all")
    public List<UserAccount> getAllUsers(){

        return userAccountRepository.getAll(UserAccount.class);
    }

    @GetMapping("/user")
    public UserAccount getUserById(@RequestParam("id") int id){

        return userAccountRepository.getById(UserAccount.class, id);
    }

    @GetMapping("/login")
    public int loginUser(@RequestParam("username") String username, @RequestParam("password") String password){

        return userAccountRepository.AuthorizeLogin(username,password);
    }
}
