package net.gsdgroup.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.gsdgroup.data.Car;
import net.gsdgroup.data.PersistedObject;
import net.gsdgroup.persistence.CarRepository;
import net.gsdgroup.persistence.PersistenceConfig;
import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarRepository carRepository;

    @GetMapping("/all")
    public List<Car> getAllCars(){

        return carRepository.getAll(Car.class);
    }

    @GetMapping("/all/userId")
    public List<Car> getAllCarsByUserId(@RequestParam("id") int id){

        return carRepository.getCarsByUserId(id);
    }

    @PostMapping("/add")
    public void addCar(@RequestPart("photo") MultipartFile photo, @RequestPart("carJson") String carJson){

        Car car = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            car = mapper.readValue(carJson, Car.class);
            byte[] bytes = IOUtils.toByteArray(photo.getInputStream());
            Blob blob = new SerialBlob(bytes);
            car.setPhoto(blob);

        } catch (IOException e) {

            e.printStackTrace();
        } catch (SQLException e){

            e.printStackTrace();
        }

        carRepository.add(car);
    }

    @PostMapping("/delete")
    public void deleteCar(@RequestParam("id") int id){

        carRepository.delete(Car.class, id);
    }

    @PostMapping("/update")
    public void updateCar(@RequestPart("photo") MultipartFile photo, @RequestPart("carJson") String carJson){

        Car car = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            car = mapper.readValue(carJson, Car.class);
            byte[] bytes = IOUtils.toByteArray(photo.getInputStream());
            Blob blob = new SerialBlob(bytes);
            car.setPhoto(blob);

        } catch (IOException e) {

            e.printStackTrace();
        } catch (SQLException e){

            e.printStackTrace();
        }
        carRepository.update(car);
    }

    @GetMapping("/car")
    public Car getCarById(@RequestParam("id") int id){

        return carRepository.getById(Car.class, id);
    }

    @GetMapping("/carPhoto")
    public String getBase64Image(@RequestParam("carId") int carId) throws SQLException, IOException {

        Blob photoBlob = carRepository.getCarPhoto(carId);
        String base64Image = null;

        if (photoBlob != null) {

            InputStream inputStream = photoBlob.getBinaryStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            byte[] imageBytes = outputStream.toByteArray();
            base64Image = Base64.getEncoder().encodeToString(imageBytes);

            inputStream.close();
            outputStream.close();

        }
        return base64Image;
    }
}
