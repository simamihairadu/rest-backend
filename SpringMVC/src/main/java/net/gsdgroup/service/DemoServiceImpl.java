package net.gsdgroup.service;

import org.springframework.stereotype.Service;

@Service
public class DemoServiceImpl implements DemoService{

    @Override
    public String getHelloMessage() {
        return "Welcome to this Demo app.";
    }

    @Override
    public String getHelloMessage(String user) {
        return "Hello " + user;
    }
}
