package net.gsdgroup.service;

public interface DemoService {

    String getHelloMessage();
    String getHelloMessage(String user);
}
