package net.gsdgroup.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Blob;

@Entity
@Table(name = "car_info")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Car extends PersistedObject {

    @Column(name = "brand", nullable = false)
    private String brand;
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private CarType carType;
    @Column(name = "price_per_day", nullable = false)
    private float pricePerDay;
    @Column(name = "warranty", nullable = false)
    private float warranty;
    @Column(name = "user_id", nullable = false)
    private int userId;
    @Lob
    @JsonIgnore
    @Column(name = "photo")
    private Blob photo;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public float getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(float pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public float getWarranty() {
        return warranty;
    }

    public void setWarranty(float warranty) {
        this.warranty = warranty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @JsonIgnore
    public Blob getPhoto() {
        return photo;
    }

    @JsonProperty
    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

}
