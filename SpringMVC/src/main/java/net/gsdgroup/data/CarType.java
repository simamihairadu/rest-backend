package net.gsdgroup.data;

public enum CarType {
    SEDAN,
    SUV,
    HATCHBACK,
    SPORT,
    TRUCK
}
