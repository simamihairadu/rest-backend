package net.gsdgroup.persistence;

import net.gsdgroup.data.Product;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Repository
public class ProductRepository extends Repository<Product>{

    @Autowired
    public ProductRepository(PersistenceConfig persistenceConfig) {
        super(persistenceConfig);
    }
}
