package net.gsdgroup.persistence;

import net.gsdgroup.data.UserAccount;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;


@org.springframework.stereotype.Repository
public class UserAccountRepository extends Repository<UserAccount> {

    @Autowired
    public UserAccountRepository(PersistenceConfig persistenceConfig) {
        super(persistenceConfig);
    }

    public int AuthorizeLogin(String username, String password){

        Session session = factory.openSession();
        UserAccount user;

        try {
            session.beginTransaction();
            Query query = session.createQuery("from UserAccount u where u.username = :username and u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);

            user = (UserAccount) query.getSingleResult();

        } finally {
            session.close();
        }

        return user.getId();
    }
}
