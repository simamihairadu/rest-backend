package net.gsdgroup.persistence;

import net.gsdgroup.data.Car;
import net.gsdgroup.data.Product;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;
import java.sql.Blob;
import java.util.List;

@org.springframework.stereotype.Repository
public class CarRepository extends Repository<Car>{

    @Autowired
    public CarRepository(PersistenceConfig persistenceConfig) {
        super(persistenceConfig);
    }

    public List<Car> getCarsByUserId(int userId){

        Session session = factory.openSession();
        List<Car> cars = null;

        try {
            session.beginTransaction();
            Query query = session.createQuery("from Car c where c.userId = :id");
            query.setParameter("id", userId);

            cars = query.getResultList();

        } finally {
            session.close();
        }

        return cars;
    }

    public Blob getCarPhoto(int carId){

        Session session = factory.openSession();
        Blob blob = null;

        try {
            session.beginTransaction();
            Query query = session.createQuery("select c.photo from Car c where c.id = :id");
            query.setParameter("id",carId);

            blob = (Blob) query.getSingleResult();
        } finally {
            session.close();
        }

        return blob;
    }

}
