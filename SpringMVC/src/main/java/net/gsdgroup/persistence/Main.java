package net.gsdgroup.persistence;

import net.gsdgroup.data.Car;
import net.gsdgroup.data.PersistedObject;
import net.gsdgroup.data.Product;
import net.gsdgroup.data.UserAccount;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class Main {
    public static void main(String[] args) {


        ProductRepository productRepository = new ProductRepository(new PersistenceConfig());
        UserAccountRepository userAccountRepository = new UserAccountRepository(new PersistenceConfig());
        CarRepository carRepository = new CarRepository(new PersistenceConfig());


        int user = userAccountRepository.AuthorizeLogin("varguard@gmail.com","parola");

        System.out.println(user);

    }
}
