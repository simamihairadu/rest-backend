package net.gsdgroup.persistence;

import net.gsdgroup.data.Car;
import net.gsdgroup.data.Product;
import net.gsdgroup.data.UserAccount;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


@Service
public class PersistenceConfig {

    private static SessionFactory factory;

    public PersistenceConfig(){

        if(factory == null){
            Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
            configuration.addAnnotatedClass(UserAccount.class);
            configuration.addAnnotatedClass(Product.class);
            configuration.addAnnotatedClass(Car.class);
            factory = configuration.buildSessionFactory();
        }
    }

    public SessionFactory getFactory() {
        return factory;
    }
}
