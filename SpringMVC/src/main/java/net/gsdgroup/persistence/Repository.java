package net.gsdgroup.persistence;

import net.gsdgroup.data.PersistedObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;


public abstract class Repository<TEntity extends PersistedObject> {

    private PersistenceConfig persistenceConfig;
    protected SessionFactory factory;

    public Repository(PersistenceConfig persistenceConfig){
        this.persistenceConfig = persistenceConfig;
        this.factory = persistenceConfig.getFactory();
    }

    public void add(TEntity entity) {

        Session session = factory.openSession();

        try {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();

        } finally {
            session.close();
        }
    }

    public void delete(Class<TEntity> type, int id){

        Session session = factory.openSession();
        try {
            session.beginTransaction();
            TEntity entity =  type.cast(session.get(type,id));
            session.delete(entity);
            session.getTransaction().commit();

        } finally {
            session.close();
        }
    }

    public void update(TEntity entity){

        Session session = factory.openSession();

        try {
            session.beginTransaction();
            session.merge(entity);
            session.getTransaction().commit();

        } finally {
            session.close();
        }
    }

    public TEntity getById(Class<TEntity> type,int id){

        Session session = factory.openSession();
        TEntity entity = null;

        try {
            session.beginTransaction();
            entity = type.cast(session.get(type,id));
            session.getTransaction().commit();

        } finally {
            session.close();
        }
        return entity;
    }

    public List<TEntity> getAll(Class<TEntity> type){

        Session session = factory.openSession();
        try{
            session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<TEntity> criteria = builder.createQuery(type);
            criteria.from(type);

            List<TEntity> list = session.createQuery(criteria).getResultList();
            return list;

        } finally {

            session.close();
        }
    }
}
